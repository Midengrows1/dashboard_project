import Vuex from 'vuex'
import Vue from 'vue'
// import { stat } from 'fs'
Vue.use(Vuex)


const state = () => ({
  ShowModal: false,
  ShowError: false,
  TasksArray: [],
  arr: [1, 2, 3, 4, 5]
})

const getters = {
  ShowModal: state => state.ShowModal,
  TasksArray: state => state.TasksArray,
  ShowError: state => state.ShowError,
  arr: state => state.arr
}
const mutations = {
  SHOWMODAL(state) {
    state.ShowModal = !state.ShowModal

  },
  ADDTASKS(state) {
    let obj = {}
    let fm = new FormData(event.target)
    fm.forEach((key, value) => {
      obj[value] = key
    })
    if (obj.title.length == 0 && obj.description.length == 0) {
      state.ShowError = true
      console.log(obj.title);
    } else {
      state.TasksArray.push(obj)
      state.ShowModal = !state.ShowModal
      state.ShowError = false
      localStorage.TasksArray = JSON.stringify(state.TasksArray)
    }

  },
  CLOSE(state) {
    state.ShowModal = false
    state.ShowError = false
  },
  // PARSE_LOCAL(state) {
  //   state.arr.push(6, 7, 8)
  //   localStorage.arr = JSON.stringify(state.arr)

  // },
  PARSEIT(state) {
    if (localStorage.TasksArray) {
      state.TasksArray = JSON.parse(localStorage.TasksArray)


    }
  }
}
const actions = {
  PARSE_LOCAL({
    commit
  }) {
    commit('PARSE_LOCAL')
  },
  PARSEIT({
    commit
  }) {
    commit('PARSEIT')
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
